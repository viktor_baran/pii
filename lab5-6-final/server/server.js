const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const http = require('http');
const socket = require('./socket');

const connectionString = 'mongodb+srv://user@pvi.z8by6bj.mongodb.net/?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority&tls=true&tlsCertificateKeyFile=./certificate/X509-cert-3391345258818356135.pem';

mongoose.connect(connectionString, {
    dbName: 'StudentDB',
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("StudentDB connected");

    const app = express();
    const httpsPort = 3000;
    const httpPort = 3080;
    const studentsRouter = require('./routes/students');
    const userRouter = require('./routes/users');
    const messagesRouter = require('./routes/messages');

    app.use(cors());
    app.use(express.json());
    app.use('/students', studentsRouter);
    app.use('/users', userRouter);
    app.use('/messages', messagesRouter);

    const server = http.createServer(app);
    socket(server);

    app.listen(httpsPort, () => console.log(`Server started on httpsPort ${httpsPort}`));
    server.listen(httpPort, () => console.log(`Socket started on httpsPort ${httpPort}`));
});