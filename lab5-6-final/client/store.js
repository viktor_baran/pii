import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'

const store = createStore({
  state() {
    return {
      authenticated: false,
      userId: "",
      userLogin: "",
    }
  },
  mutations: {
    setAuthenticated(state, value) {
      state.authenticated = value
    },
    setUserId(state, value) {
      state.userId = value
    },
    setUserLogin(state, value) {
      state.userLogin = value
    }
  },
  plugins: [createPersistedState()]
})

export default store