const addStudentModalWrapper = document.getElementById("add-student");
const deleteStudentModalWrapper = document.getElementById(
  "delete-warn-student"
);
const messageWrapper = document.getElementById("message-wrapper");
const addStudentModal = addStudentModalWrapper?.querySelector(".modal");
const addStudentForm = document.getElementById("add-student-form");
const addStudentButton = document.getElementById("btn-add");
const deleteStudentButton = document.getElementById("delete-student-btn");
const studentsTable = document.getElementById("students-table");

const notificationsButton = document.getElementById("notifications-btn");

notificationsButton.ondblclick = () => {
  notificationsButton.animate(
    [{ transform: "scale(1.2)" }, { transform: "scale(1)" }],
    {
      duration: 500,
      iterations: 1,
    }
  );
};

const deleteModalButton = document.getElementById("delete-modal-btn");

let currentStudent = null;

function hideModal() {
  addStudentModalWrapper.classList.add("hidden");
}

function showModal() {
  addStudentModalWrapper.classList.remove("hidden");
}

function hideDeleteModal() {
  deleteStudentModalWrapper.classList.add("hidden");
}

function showDeleteModal() {
  deleteStudentModalWrapper.classList.remove("hidden");
}

function createStudent({ group, name, surname, birthday, gender }) {
  console.log(group, name, surname, birthday, gender);
  const student = document.createElement("tr");

  // create new elements
  const tr = document.createElement("tr");
  const checkboxTd = document.createElement("td");
  const groupTd = document.createElement("td");
  const nameSurnameTd = document.createElement("td");
  const genderTd = document.createElement("td");
  const birthdayTd = document.createElement("td");
  const status = document.createElement("td");
  const checkbox = document.createElement("input");
  const actionsContainer = document.createElement("td");
  const editButton = document.createElement("button");
  const deleteButton = document.createElement("button");

  // set the attributes and content for the new elements
  checkbox.type = "checkbox";
  checkboxTd.appendChild(checkbox);
  groupTd.textContent = group;
  nameSurnameTd.textContent = `${name} ${surname}`;
  genderTd.textContent = gender;
  birthdayTd.textContent = birthday;
  status.textContent = "Status";
  editButton.innerHTML = '<i class="fa fa-pencil" aria-hidden="true"></i>';
  deleteButton.innerHTML = '<i class="fa fa-trash-o" aria-hidden="true"></i>';
  deleteButton.addEventListener("click", (e) => {
    showDeleteModal();
    const cancelModalButton = document.getElementById("cancel-modal-btn");
    console.log(cancelModalButton);
    cancelModalButton.onclick = hideDeleteModal;
    deleteModalButton.onclick = () => {
      currentStudent.remove();
      hideDeleteModal();
    };
    currentStudent = e.target.closest("tr");
  });
  actionsContainer.classList.add("flex");
  actionsContainer.appendChild(editButton);
  actionsContainer.appendChild(deleteButton);

  // append the new elements to the row
  tr.appendChild(checkboxTd);
  tr.appendChild(groupTd);
  tr.appendChild(nameSurnameTd);
  tr.appendChild(genderTd);
  tr.appendChild(birthdayTd);
  tr.appendChild(status);
  tr.appendChild(actionsContainer);

  // append the row to the table
  studentsTable.appendChild(tr);
  return student;
}

addStudentButton.onclick = showModal;
addStudentForm.onsubmit = (e) => {
  e.preventDefault();
  const formData = new FormData(e.target);
  const student = createStudent(Object.fromEntries(formData));
  hideModal();
};

document.addEventListener("mousedown", (e) =>
  handleClickOutside(e, addStudentModal, hideModal)
);

function handleClickOutside({ target }, element, callback) {
  if (element && target instanceof HTMLElement && !element.contains(target)) {
    callback();
  }
}

deleteStudentButton.onclick = (e) => {
  showDeleteModal();
  const cancelModalButton = document.getElementById("cancel-modal-btn");
  console.log(cancelModalButton);
  cancelModalButton.onclick = hideDeleteModal;
  deleteModalButton.onclick = () => {
    currentStudent.remove();
    hideDeleteModal();
  };
  currentStudent = e.target.closest("tr");
};

function myFunction() {
  document.getElementById("message-wrapper").style.display = "block";
}
function myFunction2() {
  document.getElementById("message-wrapper").style.display = "none";
}
function myFunctionA() {
  document.getElementById("avatar-wrapper").style.display = "block";
}
function myFunctionA2() {
  document.getElementById("avatar-wrapper").style.display = "none";
}
