const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
    const token = req.header('Authorization');
    if (!token) {
        return res.status(401).send('Access Denied');
    }

    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = verified;
        next();
    } catch (err) {
        res.status(400).send('Invalid Token');
    }
}

// SIGN UP
async function signUpUser(req, res) {
    const { login, password } = req.body;

    try {
        const user = await User.findOne({ login });
        if (user) {
            return res.status(409).json({ message: 'User already exists.' });
        }

        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = new User({
            login: login,
            password: hashedPassword
        });
        await newUser.save();

        res.status(201).json({ message: 'User created successfully!' });
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: 'Server error.' });
    }
}

// SIGN IN
async function signInUser(req, res) {
    try {
        const user = await User.findOne({ login: req.body.login });
        if (!user) {
            return res.status(400).send('Invalid login or password.');
        }

        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword) {
            return res.status(400).send('Invalid login or password.');
        }

        require('dotenv').config();
        const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
        res.status(200).header('Authorization', `Bearer ${token}`).json({ user: user, token: token });
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: 'Server error.' });
    }
}

// READ ALL
async function getAllUsers(req, res) {
    try {
      const users = await User.find({});
      res.send(users);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  }

module.exports = {
    signUpUser,
    signInUser,
    getAllUsers,
};